#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>

static void username(void) {
    struct passwd* p;
    if ((p = getpwuid(getuid()))) {
        fputs(p->pw_name, stdout);
    }
}

static void host(void) {
    char h[2048];
    if (!gethostname(h, sizeof(h))) {
        h[sizeof(h) - 1] = 0;
        fputs(h, stdout);
    }
}

static void curdir(void) {
    char cwd[2048];
    struct passwd* p;
    size_t n;
    const char* home = NULL;
    if ((p = getpwuid(getuid()))) {
        home = p->pw_dir;
    }
    if (getcwd(cwd, sizeof(cwd))) {
        if (home && !strncmp(cwd, home, n = strlen(home)) && (cwd[n] == '/' || !cwd[n])) {
            fputc('~', stdout);
            fputs(cwd + n, stdout);
        } else {
            fputs(cwd, stdout);
        }
    } else if (errno == ERANGE) {
        fputs("(wd too long)", stdout);
    }
}

enum ColorType {
    COLOR_PAL8 = 0,
    COLOR_PAL256 = 5,
    COLOR_RGB = 2
};

static void setcolor(unsigned int mode, const unsigned char* clr) {
    fputs("\x1B[", stdout);
    if (!*clr) {
        fprintf(stdout, "%d", mode + clr[1]);
    } else {
        unsigned int i, n = (16U - 2U * (*clr)) / 3U;
        fprintf(stdout, "%u", mode + 8U);
        for (i = 0; i < n; i++) {
            fprintf(stdout, ";%u", (unsigned int)clr[i]);
        }
    }
    fputc('m', stdout);
}

static void setfg(const unsigned char* clr) {
    setcolor(30, clr);
}

static void setbg(const unsigned char* clr) {
    setcolor(40, clr);
}

static char* parse_color(const char* fmt, unsigned char* clr) {
    const char* pal8[] = {"black", "red", "green", "yellow", "blue", "magenta", "cyan", "white"};
    const char* end;
    unsigned int i, n;
    if (*fmt++ != '{') return NULL;
    for (end = fmt; *end && *end != '}'; end++);
    if (!*end) return NULL;
    if (end - fmt == 1 && *fmt >= '0' && *fmt <= '9' && *fmt != '8') {
        clr[0] = COLOR_PAL8;
        clr[1] = *fmt - '0';
        return (char*)end;
    }
    for (i = 0; i < 8; i++) {
        if (!strncmp(fmt, pal8[i], end - fmt)) {
            clr[0] = COLOR_PAL8;
            clr[1] = i;
            return (char*)end;
        }
    }
    if (!strncmp(fmt, "pal:", 4)) {
        clr[0] = COLOR_PAL256;
        n = 1;
    } else if (!strncmp(fmt, "rgb:", 4)) {
        clr[0] = COLOR_RGB;
        n = 3;
    } else {
        return NULL;
    }
    fmt += 3;
    for (i = 1; i <= n; i++) {
        long v = strtol(fmt + 1, (char**)&fmt, 10);
        if (v < 0 || v > 255 || *fmt != ((i == n) ? '}' : ',')) return NULL;
        clr[i] = v;
    }
    if (fmt != end) return NULL;
    return (char*)end;
}

static void segment(const unsigned char* clr) {
    static unsigned char last[4] = {-1};
    const unsigned char defclr[] = {COLOR_PAL8, 9};
    const char* segsep = "\xEE\x82\xB0";
    const char* term;
    if (last[0] != (unsigned char)-1) {
        setfg(last);
        setbg(clr);
        if ((term = getenv("TERM")) && !strcmp(term, "linux")) {
            segsep = "\xE2\x96\xB6";
        }
        fputs(segsep, stdout);
    } else {
        setbg(clr);
    }
    setfg(defclr);
    memcpy(last, clr, sizeof(last));
}

int main(int argc, char** argv) {
    char *fmt, *end;
    int condition = 1, inCondition = 0, c;
    unsigned char clr[4];

#ifdef HAVE_GIT
    void git_env(void);
    git_env();
#endif

    if (argc > 2 || !(fmt = ((argc >= 2) ? argv[1] : getenv("PROMPT_FORMAT"))) || !*fmt) {
        fputs("Usage: prompt [format]\n", stderr);
        return 1;
    }

    for (; *fmt; fmt++) {
        switch (*fmt) {
            case '\\':
                switch (*++fmt) {
                    case 'a': c = '\a'; break;
                    case 'n': c = '\n'; break;
                    case 't': c = '\t'; break;
                    case 'e': c = '\x1B'; break;
                    default:  c = *fmt; break;
                }
                if (condition) fputc(c, stdout);
                break;
            case '%':
                switch (*++fmt) {
                    case 'u': if (condition) username(); break;
                    case 'h': if (condition) host(); break;
                    case 'd': if (condition) curdir(); break;
                    case 'b': if (!(end = parse_color(fmt + 1, clr))) goto percent_default; if (condition) setbg(clr); fmt = end; break;
                    case 'f': if (!(end = parse_color(fmt + 1, clr))) goto percent_default; if (condition) setfg(clr); fmt = end; break;
                    case 'r': if (condition) fputs("\x1B[0m", stdout); break;
                    case 'B': if (condition) fputs("\x1B[1m", stdout); break;
                    case 'R': if (condition) fputs("\x1B[22m", stdout); break;
                    percent_default:
                    default:
                        if (condition) {
                            fputc('%', stdout);
                            fputc(*fmt, stdout);
                        }
                }
                break;
            case '>':
                if ((end = parse_color(fmt + 1, clr))) {
                    if (condition) segment(clr);
                    fmt = end;
                }
                break;
            case '$':
                if (*++fmt == '{') {
                    char *end, *var;
                    for (end = ++fmt; *end && *end != '}'; end++);
                    if (*end) {
                        *end = 0;
                        if ((var = getenv(fmt))) {
                            if (condition) fputs(var, stdout);
                        }
                        fmt = end;
                    } else {
                        fmt = end - 1;
                    }
                }
                break;
            case '?':
                if (*++fmt == '{') {
                    int asnum = 0, not = 0, param = 1;
                    char *end, *var;
                    do {
                        switch (*++fmt) {
                            case '#': asnum = 1; break;
                            case '!': not = 1; break;
                            default:  param = 0; break;
                        }
                    } while (param);
                    for (end = fmt; *end && *end != '}'; end++);
                    if (*end) {
                        *end = 0;
                        if (condition) {
                            condition = (var = getenv(fmt)) && *var;
                            if (asnum && condition) {
                                while (*var && *var == '0') var++;
                                condition = (*var != 0);
                            }
                            if (not) condition = !condition;
                        }
                        fmt = end;
                        if (fmt[1] == '{') {
                            inCondition = 1;
                            fmt++;
                        } else if (fmt[1] != '?') {
                            condition = 1;
                        }
                    } else {
                        fmt = end - 1;
                    }
                }
                break;
            case '}':
                if (inCondition) {
                    inCondition = 0;
                    condition = 1;
                    break;
                }
            default:
                if (condition) fputc(*fmt, stdout);
        }
    }

    return 0;
}
