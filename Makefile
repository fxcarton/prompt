CFLAGS ?= -std=c89 -pedantic -D_POSIX_C_SOURCE=200112L -Wall -O2
PREFIX ?= /usr

prompt: prompt.c $(if $(GIT),git.c)
prompt: CFLAGS += $(if $(GIT),-DHAVE_GIT)

.PHONY: clean install
clean:
	rm -f prompt
BIN := $(if $(DESTDIR),$(DESTDIR)/)$(PREFIX)/$(or $(BINDIR),bin)
install: prompt
	mkdir -p $(BIN)
	cp $< $(BIN)
