#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

static int do_exec(int* status, const char* env, int schld, char* const* argv) {
    pid_t pid;
    int p[2], ok = 1;
    if (env && pipe(p)) return 0;
    switch (pid = fork()) {
        case -1:
            if (env) {
                close(p[0]);
                close(p[1]);
            }
            return 0;
        case 0:
            close(0);
            close(1);
            close(2);
            if (env && dup2(p[1], 1) == -1) return 1;
            if (env) {
                close(p[0]);
                close(p[1]);
            }
            execvp(*argv, argv);
            exit(1);
            return 1;
    }
    if (env) {
        char buffer[1024], *ptr;
        size_t n = 0;
        ssize_t r;
        close(p[1]);
        do {
            if ((r = read(p[0], buffer + n, sizeof(buffer) - n - 1)) < 0) {
                ok = 0;
            } else {
                n += r;
            }
        } while (ok && r && n < sizeof(buffer) - 1);
        close(p[0]);
        if (ok) {
            buffer[n] = 0;
            for (ptr = buffer; *ptr && *ptr != '\n'; ptr++);
            *ptr = 0;
            ok = !setenv(env, buffer, 1);
        }
        if (schld) kill(pid, SIGCHLD);
    }
    if (waitpid(pid, status, 0) != pid || !(WIFEXITED(*status) || WIFSIGNALED(*status))) {
        kill(pid, SIGKILL);
        ok = 0;
    }
    return ok;
}

void git_env(void) {
    int status;
    char* gitIsInsideWorkTree[] = {"git", "rev-parse", "--is-inside-work-tree", NULL};
    char* gitDirty[] = {"git", "status", "--porcelain", NULL};
    char* gitRef[] = {"git", "symbolic-ref", "HEAD", NULL};
    char* gitRev[] = {"git", "rev-parse", "--short", "HEAD", NULL};
    const char* v;

    if (!do_exec(&status, NULL, 0, gitIsInsideWorkTree) || !WIFEXITED(status) || WEXITSTATUS(status)) return;
    if (do_exec(&status, "GIT_DIRTY", 1, gitDirty)) {
        setenv("GIT_DIRTY", ((v = getenv("GIT_DIRTY")) && *v) ? "1" : "0", 1);
    }
    if (do_exec(&status, "GIT_REF", 0, gitRef) && WIFEXITED(status) && !WEXITSTATUS(status)) {
        if ((v = getenv("GIT_REF"))) {
            if (!strncmp(v, "refs/heads/", 11)) {
                setenv("GIT_REF", v + 11, 1);
            }
        }
    } else {
        do_exec(&status, "GIT_REV", 0, gitRev);
    }
}
